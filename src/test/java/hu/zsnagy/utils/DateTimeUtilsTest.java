package hu.zsnagy.utils;

import org.junit.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

public class DateTimeUtilsTest {

    @Test
    public void it_formats_the_date_with_offset(){
        LocalDate localDate = LocalDate.parse("2020-12-06");

        assertThat(DateTimeUtils.getDateStringWithOffset(localDate, 3)).isEqualTo("2020-12-09");
    }

}