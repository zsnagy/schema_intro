package hu.zsnagy.services;

import hu.zsnagy.models.WeatherResult;
import hu.zsnagy.repositories.WeatherDataRepository;
import hu.zsnagy.repositories.entities.WeatherData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WeatherHelperTest {
    @Mock
    private WeatherDataRepository repository;

    @InjectMocks
    private WeatherHelper weatherHelper;

    @Test
    public void it_collects_the_persisted_data() {
        weatherHelper.getWeatherData();
        verify(repository).findAll();
    }

    @Test
    public void it_does_not_store_the_data_if_its_already_in_the_db() {
        when(repository.findOneByDate("2020-12-06")).thenReturn(new WeatherData("2020-12-06", 12));

        weatherHelper.storeWeatherData(Collections.singletonList(new WeatherResult("2020-12-06", 12)));

        verify(repository, never()).saveAndFlush(any(WeatherData.class));
    }

    @Test
    public void it_saves_the_data_if_its_not_in_the_db() {
        when(repository.findOneByDate("2020-12-06")).thenReturn(null);

        weatherHelper.storeWeatherData(Collections.singletonList(new WeatherResult("2020-12-06", 12)));

        verify(repository).saveAndFlush(any(WeatherData.class));
    }
}