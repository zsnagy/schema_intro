package hu.zsnagy.services;

import hu.zsnagy.models.WeatherResult;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StartupApplicationListenerTest {

    @Mock
    private WeatherHelper weatherHelper;
    @Mock
    private WeatherReader reader;

    @Test
    public void it_collects_and_persists_the_weather_forecast() {
        List<WeatherResult> weatherResults = Arrays.asList(
                new WeatherResult("2020-12-06", 12),
                new WeatherResult("2020-12-07", 6));
        when(reader.getWeatherData()).thenReturn(weatherResults);

        StartupApplicationListener listener = new StartupApplicationListener(reader, weatherHelper);
        listener.onApplicationEvent(null);

        verify(weatherHelper).storeWeatherData(weatherResults);
    }
}