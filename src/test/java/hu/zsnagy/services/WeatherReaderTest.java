package hu.zsnagy.services;

import hu.zsnagy.configs.ConfigProperties;
import hu.zsnagy.utils.HttpUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WeatherReaderTest {
    @Mock
    private ConfigProperties configProperties;

    @Mock
    private HttpUtils httpUtils;

    @Test
    public void it_can_gather_weather_forecast_data() {
        when(configProperties.getApiUrl()).thenReturn("http://localhost?key=%s");
        when(configProperties.getApiKey()).thenReturn("abc");
        String rawResult = "{\"lat\":47.5,\"lon\":19.07,\"timezone\":\"Europe/Budapest\",\"timezone_offset\":3600,\"daily\":[{\"dt\":1607248800,\"sunrise\":1607235410,\"sunset\":1607266396,\"temp\":{\"day\":10.68,\"min\":6.89,\"max\":12.27,\"night\":8.22,\"eve\":10.31,\"morn\":7.45},\"feels_like\":{\"day\":6.06,\"night\":2.57,\"eve\":5.31,\"morn\":3.96},\"pressure\":1012,\"humidity\":70,\"dew_point\":5.49,\"wind_speed\":5.12,\"wind_deg\":126,\"weather\":[{\"id\":803,\"main\":\"Clouds\",\"description\":\"broken clouds\",\"icon\":\"04d\"}],\"clouds\":75,\"pop\":0,\"uvi\":0.9},{\"dt\":1607335200,\"sunrise\":1607321874,\"sunset\":1607352783,\"temp\":{\"day\":6.86,\"min\":5.46,\"max\":8.18,\"night\":6.03,\"eve\":5.86,\"morn\":7.74},\"feels_like\":{\"day\":1.79,\"night\":2.23,\"eve\":2.58,\"morn\":2.07},\"pressure\":1010,\"humidity\":74,\"dew_point\":2.67,\"wind_speed\":4.98,\"wind_deg\":121,\"weather\":[{\"id\":500,\"main\":\"Rain\",\"description\":\"light rain\",\"icon\":\"10d\"}],\"clouds\":100,\"pop\":1,\"rain\":5.27,\"uvi\":0.09},{\"dt\":1607421600,\"sunrise\":1607408335,\"sunset\":1607439173,\"temp\":{\"day\":6.79,\"min\":3.59,\"max\":7.96,\"night\":3.59,\"eve\":6.59,\"morn\":5.7},\"feels_like\":{\"day\":2.26,\"night\":-1.6,\"eve\":1.42,\"morn\":1.67},\"pressure\":1013,\"humidity\":78,\"dew_point\":3.37,\"wind_speed\":4.39,\"wind_deg\":128,\"weather\":[{\"id\":804,\"main\":\"Clouds\",\"description\":\"overcast clouds\",\"icon\":\"04d\"}],\"clouds\":88,\"pop\":0.38,\"uvi\":0.56}]}";
        when(httpUtils.callHttpGet("http://localhost?key=abc")).thenReturn(rawResult);

        WeatherReader reader = new WeatherReader(httpUtils, configProperties);
        assertThat(reader.getWeatherData()).hasSize(3);
    }

}