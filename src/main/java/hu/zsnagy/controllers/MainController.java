package hu.zsnagy.controllers;

import hu.zsnagy.repositories.entities.WeatherData;
import hu.zsnagy.services.WeatherHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
public class MainController {

    @Autowired
    private WeatherHelper weatherHelper;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("cityName", "Budapest");

        Map<String, Integer> graphData = new TreeMap<>();
        List<WeatherData> dataList = weatherHelper.getWeatherData();
        dataList.forEach(weatherData -> graphData.put(weatherData.getDate(), weatherData.getDegree()));
        model.addAttribute("chartData", graphData);

        return "index";
    }
}
