package hu.zsnagy.models;

public class WeatherResult {

    private String date;
    private Integer degree;

    public WeatherResult(String date, Integer degree) {
        this.date = date;
        this.degree = degree;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }
}
