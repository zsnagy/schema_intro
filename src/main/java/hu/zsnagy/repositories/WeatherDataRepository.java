package hu.zsnagy.repositories;

import hu.zsnagy.repositories.entities.WeatherData;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WeatherDataRepository extends JpaRepository<WeatherData, Integer> {

    WeatherData findOneByDate(String date);
}

