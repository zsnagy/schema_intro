package hu.zsnagy.repositories.entities;

import javax.persistence.*;

@Entity
@Table(name = "weather_data")
public class WeatherData {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "measure_date")
    private String date;

    @Column(name = "degree")
    private Integer degree;

    public WeatherData() {
    }

    public WeatherData(String date, Integer degree) {
        this.date = date;
        this.degree = degree;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Integer getDegree() {
        return degree;
    }

    public void setDegree(Integer degree) {
        this.degree = degree;
    }
}
