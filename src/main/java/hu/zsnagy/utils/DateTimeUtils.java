package hu.zsnagy.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateTimeUtils {
    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public static String getDateStringWithOffset(LocalDate date, int offset) {
        return DATE_TIME_FORMATTER.format(date.plusDays(offset));
    }
}
