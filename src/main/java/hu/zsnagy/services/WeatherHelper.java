package hu.zsnagy.services;

import hu.zsnagy.models.WeatherResult;
import hu.zsnagy.repositories.WeatherDataRepository;
import hu.zsnagy.repositories.entities.WeatherData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeatherHelper {

    private WeatherDataRepository weatherRepository;

    @Autowired
    public WeatherHelper(WeatherDataRepository weatherRepository) {
        this.weatherRepository = weatherRepository;
    }

    public List<WeatherData> getWeatherData() {
        return weatherRepository.findAll();
    }

    public void storeWeatherData(List<WeatherResult> forecast) {
        forecast.forEach(this::insertIfNotExists);
    }

    private void insertIfNotExists(WeatherResult weatherResult) {
        WeatherData persisted = weatherRepository.findOneByDate(weatherResult.getDate());
        if (persisted == null) {
            WeatherData data = new WeatherData(weatherResult.getDate(), weatherResult.getDegree());
            weatherRepository.saveAndFlush(data);
        }
    }
}
