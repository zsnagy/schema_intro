package hu.zsnagy.services;

import hu.zsnagy.models.WeatherResult;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class StartupApplicationListener implements ApplicationListener<ContextRefreshedEvent> {

    private WeatherReader weatherReader;
    private WeatherHelper weatherHelper;

    public StartupApplicationListener(WeatherReader weatherReader, WeatherHelper weatherHelper) {
        this.weatherReader = weatherReader;
        this.weatherHelper = weatherHelper;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        List<WeatherResult> weatherForecast = weatherReader.getWeatherData();
        weatherHelper.storeWeatherData(weatherForecast);
    }
}