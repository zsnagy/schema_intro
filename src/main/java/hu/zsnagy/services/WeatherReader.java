package hu.zsnagy.services;

import hu.zsnagy.configs.ConfigProperties;
import hu.zsnagy.models.WeatherResult;
import hu.zsnagy.utils.DateTimeUtils;
import hu.zsnagy.utils.HttpUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherReader {

    private HttpUtils httpUtils;
    private ConfigProperties configProperties;

    @Autowired
    public WeatherReader(HttpUtils httpUtils, ConfigProperties configProperties) {
        this.httpUtils = httpUtils;
        this.configProperties = configProperties;
    }

    public List<WeatherResult> getWeatherData() {
        String url = String.format(configProperties.getApiUrl(), configProperties.getApiKey());

        String rawResult = httpUtils.callHttpGet(url);

        JSONObject result = new JSONObject(rawResult);
        JSONArray resultArray = (JSONArray) result.get("daily");

        List<WeatherResult> results = new ArrayList<>();
        for (int i = 0; i < resultArray.length(); i++) {
            WeatherResult weatherResult = toWeatherResult(i, (JSONObject) resultArray.get(i));
            if (weatherResult != null) {
                results.add(weatherResult);
            }
        }
        return results;
    }

    private WeatherResult toWeatherResult(int offset, JSONObject result) {
        try {
            JSONObject tempResult = (JSONObject) result.get("temp");
            Integer degreee = getDegreeAsInt(tempResult);
            return new WeatherResult(DateTimeUtils.getDateStringWithOffset(LocalDate.now(), offset), degreee);
        } catch (Exception e) {

        }
        return null;
    }

    private Integer getDegreeAsInt(JSONObject tempResult) {
        try {
            BigDecimal rawDegreee = (BigDecimal) tempResult.get("day");
            return rawDegreee.intValue();
        } catch (Exception e) {

        }

        try {
            BigInteger rawDegreee = (BigInteger) tempResult.get("day");
            return rawDegreee.intValue();
        } catch (Exception e) {

        }
        return 0;

    }


}
