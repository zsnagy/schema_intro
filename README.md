## Starschema intro

This intro app is for demonstrating spring-boot, Thymeleaf, Vue.js and H2

### What does this app do?
It collects the 7 days weather forecast, stores in the H2 database and provides a page where it can be seen.

### Should I config something?
Yes, please modify the application.properties in the src/main/resources folder.
You should change the _spring.datasource.url_ to a place where your have a few kBytes of space.
Or if you not intend to examine the database you may set it to `jdbc:h2:mem:testdb`

### How to run?
`mvn spring-boot:run`

### Where can I see?
Use your browser to see the page here: http://localhost:8080/
